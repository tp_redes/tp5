from flask import Flask, jsonify, request, abort
import random
from students import students
from datetime import datetime

app = Flask(__name__)

@app.route('/', methods=['GET'])
def ping():
     return jsonify({"response":"Hello world"})

@app.route('/alumnos', methods=['GET', 'POST','DELETE'])
def studentsHandler():
    if request.method == 'GET':
        return jsonify({"students":students})
    if request.method == 'POST':
        name = request.json['name']
        lastName = request.json['lastName']
        age = request.json['age']
        ID = datetime.now()
        student ={
            "ID": ID,
            "Name": name,
            "LastName": lastName,
            "Age":age
        }
        students.append(student)
        return jsonify({"student":student})    

@app.route('/alumnos/<int:student_id>', methods=['DELETE'])
def deleteStudents(student_id):
    
    if request.method =='DELETE':
        for student in students:
            if student['ID'] == student_id:
                students.remove(student)  
                return   jsonify({"response":"Remove succesfully"})
        else:
            abort(404)

@app.errorhandler(404) 
def not_found(e): 
  return   jsonify({"response":"Student Not Found"}),404


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
