init: setup delete_routes conf_routes conf_flask

setup:
	docker-compose up -d

clean:
	docker-compose down;

delete_routes:
	docker exec -ti r1 ip route del default;
	docker exec -ti root ip route del default via 192.169.0.1;
	docker exec -ti pc1 ip route del default via 192.169.1.1;
	docker exec -ti pc2 ip route del default via 192.169.1.1;
	docker exec -ti pc3 ip route del default via 192.169.2.1;
	docker exec -ti pedu ip route del default via 192.169.1.1;
	docker exec -ti sedu ip route del default via 192.169.1.1;
	docker exec -ti pcom ip route del default via 192.169.2.1;

conf_routes:
	docker exec -ti root ip route add default via 192.169.0.10;
	docker exec -ti pc1 ip route add default via 192.169.1.10;
	docker exec -ti pc2 ip route add default via 192.169.1.10;
	docker exec -ti pc3 ip route add default via 192.169.2.10;
	docker exec -ti pedu ip route add default via 192.169.1.10;
	docker exec -ti sedu ip route add default via 192.169.1.10;
	docker exec -ti pcom ip route add default via 192.169.2.10;

conf_flask:
	docker exec -ti pc2 python FlaskApp/src/app.py;

check_connectivity:
	docker exec -ti pc1 ping -c 3 192.169.1.10;
	docker exec -ti pc1 ping -c 3 192.169.1.12;
	docker exec -ti pc1 ping -c 3 192.169.1.13;
	docker exec -ti pc1 ping -c 3 192.169.1.14;
	docker exec -ti pc1 ping -c 3 192.169.0.10;
	docker exec -ti pc1 ping -c 3 192.169.0.11;
	docker exec -ti pc1 ping -c 3 192.169.2.10;
	docker exec -ti pc1 ping -c 3 192.169.2.11;
	docker exec -ti pc1 ping -c 3 192.169.2.12;
	
check_dns:
	docker exec -ti pc1 nslookup pc2.edu;
	docker exec -ti pc1 nslookup pc3.com;
	docker exec -ti pc3 nslookup pc1.edu;
	docker exec -ti pc3 nslookup pc2.edu;

check_cache:
	docker exec -ti pc1 drill pc3.com | grep "Query time";

check_api:
	docker exec -ti pc1 curl -4 http://pc2.edu:5000;
	docker exec -ti pc3 curl -4 http://pc2.edu:5000;

API_GET: 
	docker exec -ti pc1 curl -4 http://pc2.edu:5000/alumnos;

API_POST: 
	docker exec -ti pc1 curl -X POST -H 'Content-Type: application/json' -d '{"name":"Diego Armando", "lastName":"Maradona", "age":"50"}' http://pc2.edu:5000/alumnos;

API_DELETE: 
	docker exec -ti pc1 curl -X DELETE http://pc2.edu:5000/alumnos/1;
	docker exec -ti pc3 curl -X DELETE http://pc2.edu:5000/alumnos/3;