# Quagga ZEBRA
#
# VERSION 0.0.1
# Cmd = docker build -t zebra -f zebra.dockerfile .

FROM alpine:latest

LABEL maintainer.name="Lucas Monsierra" \
      maintainer.email="lucasmonsierra@gmail.com" \
      version="0.0.1" \
      description="Quagga ZEBRA - apline based image"

RUN apk update
RUN apk add supervisor quagga heimdal tcpdump

RUN echo "zebra=yes" >> /etc/quagga/daemons

ENTRYPOINT ["/usr/bin/supervisord"]
